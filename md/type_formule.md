# Représentation des formules logiques en Python

**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.2.2, p321

## Grammaire BNF des formules logiques
Nous définissons les formules logiques selon la grammaire 
[BNF](https://fr.wikipedia.org/wiki/Forme_de_Backus-Naur) suivante :

Formule := <BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Vrai```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Faux```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Var<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Non<Formule>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Et<Formule, Formule>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Ou<Formule, Formule>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Implique<Formule, Formule>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```Equivalent<Formule, Formule>```
 
## Tuples comme représentation des formules logiques
Comme le type Somme de OCaml n'existe pas en Python nous utiliserons des 
[S-expression](https://fr.wikipedia.org/wiki/S-expression) à la place.<BR/>
Le type Tuple de Python est le plus approprié pour cela.
    
Ainsi la formule ```(P ==> Q) <==> ((Non P) \/ Q)``` pourra s'écrire : <BR/>
```'Equivalent', (('Implique', (('Var', 'P'), ('Var', 'Q'))), ('Ou', (('Non', ('Var', 'P')), ('Var', 'Q'))))```
    
## Fonction is_tformule
OCaml est un langage au 
[typage statique](https://fr.wikipedia.org/wiki/Typage_statique#R%C3%A9solution,_autres_difficult%C3%A9s),
Python est un langage au 
[typage dynamique](https://fr.wikipedia.org/wiki/Typage_dynamique), la fonction **is_tformule**<BR/> aide au 
développement en vérifiant récursivement que son son paramètre est bien du type tformule tel que défini précédemment.



```python
"""
  Fonctions de vérification du type tformule (Formule logique)
"""

def is_tformule (tformule):
    '''
    * La fonction is_tformule vérifie le type de son paramètre tformule.
    * Si tformule est un tuple respectant la grammaire BNF des formules logiques la 
    fonction renvoie True sinon elle renvoie False.
    * Ce tuple est constitué d'un tag de type chaîne de caractère et d'une Valeur.
      + Les tag peuvent être 'Vrai'|'Faux'|'Var'|'Non'|'Et'|'Ou'|'Implique'|'Equivalent'
      + Les valeurs peuvent être :
        - None, pour les tags 'Vrai' et 'Faux'
        - Un string, pour un identifiant de proposition
        - Une formule, tel que sa représentation est entendue dans notre modélisation
        - Un couple de formule
    * La fonction renvoie False si la structure est incohérente
    * La fonction renvoie Vrai si la structure est valide
    * Une exception ValueError est levée si l'argument tformule n'est pas une paire
    '''
    try:
        tag, val = tformule
    except TypeError as err:
        raise err

    if tag in ('Vrai', 'Faux') and val is None:
        return True

    if tag == 'Var' and isinstance(val, str):
        return True
        
    if tag == 'Non' and is_tformule(val):
        return True
            
    try :
        val1, val2 = val
    except TypeError as err:
        raise err
        
    if tag in ('Et', 'Ou', 'Implique', 'Equivalent') and is_tformule(val1) and is_tformule(val2):
        return True        
        
    return False
```


```python
class TypeCheckFormuleError(Exception):
    """
    Exception TypeCheckFormuleError pour la fonction typecheck 
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)
    
def typecheck_formule(tformule):
    """
    * La fonction typecheck_formule vérifie que son paramètre tformule est
      un tuple bien structuré respectant la grammaire des formules logiques.
    * Si ce n'est pas le cas elle lève une exception TypeCheckFormuleError
    """
    try:
        if not is_tformule(tformule):
            raise TypeCheckFormuleError(tformule)
    except TypeError as err:
        raise TypeCheckFormuleError(tformule) from err
```
