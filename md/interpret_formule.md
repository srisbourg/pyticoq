# Interprétation des formules logiques en Python

**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.2.4, p323

Les valeurs de vérité des variables sont stockées dans un dictionnaire.<BR />
Les clés du dictionnaire sont les identifiants (les noms) des variables propositionnelles
et la valeur associée est leur valeur de vérité.<BR />
On considère donc un dictionnaire dont les clés sont des chaînes de caractères qui
sont associées aux valeurs booléennes ```True``` ou ```False```.


```python
"""
Fonctions d'interprétation des formules par calcul des tables de vérité
"""

from ipynb.fs.full.type_formule import typecheck_formule
```


```python
class AssocError(Exception):
    """
    Exception AssocError pour la fonction assoc
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)

def assoc (cle, dico):
    '''
    * La fonction assoc recherche la clé cle dans le dictionnaire dico et
    renvoie la valeur associée à cette clé si elle existe.
    * Le dictionnaire ne contient que des valeurs booléennes.
    * Si la clé n'existe pas dans le dictionnaire ou si la valeur
    trouvée n'est pas un booléen alors la fonction renvoie une 
    exception AssocError.
    '''
    try:
        val = dico[cle]
    except TypeError as err:
        raise AssocError((cle, dico)) from err
    if val not in (True, False):
        raise AssocError((cle, dico))
    return val 
```

La fonction **assoc** est utilisée par la fonction **eval_tf**
pour évaluer une formule en fonction des valeurs de verité des
variables propositionnelles stockée dans un dictionnaire.


```python
class EvalError(Exception):
    """
    Exception EvalError pour la fonction eval_tf
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)

def eval_tf (dict_assoc, tformule):
    '''
    * La fonction eval_tf calcule la valeur booléenne d'une
      formule logique représentée par un tuple tformule 
      et un dictionnaire dict_assoc
      contenant les valeurs de vérité de ses atomes 
      (variables propositionnelles).
    * La fonction vérifie que la formule est bien formée à l'aide
      des fonctions définies dans le module type_formule.
    * Si la formule est bien formée l'évaluation débute sinon
      la fonction lève une exception EvalError.
    * Si la formule contient un atome qui n'est pas présent dans
      le dictionnaire la fonction lève une exception EvalError.
    * Si l'évaluation échoue la fonction lève une exception EvalError.
    '''
    typecheck_formule(tformule)
   
    # evaluation
    try: 
        tag, val = tformule
    except TypeError as err :
        raise EvalError(tformule) from err

    if tag == 'Vrai':
        return True

    if tag == 'Faux':
        return False

    if tag == 'Var':
        return assoc(val, dict_assoc)

    if tag == 'Non':
        return not eval_tf(dict_assoc, val)

    try:
        val1, val2 = val
    except TypeError as err:
        raise EvalError(val) from err

    if tag == 'Et':
        return eval_tf(dict_assoc, val1) and eval_tf(dict_assoc, val2)

    if tag == 'Ou':
        return eval_tf(dict_assoc, val1) or eval_tf(dict_assoc, val2)

    if tag == 'Implique':
        return (not eval_tf(dict_assoc, val1))  or eval_tf(dict_assoc, val2) # Rappel formules de Morgan

    if tag == 'Equivalent':
        return eval_tf(dict_assoc, val1) == eval_tf(dict_assoc, val2)

    raise EvalError((dict_assoc, tformule))
```

# Tautologie et Contradiction

**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.2.5, p324

## Enumération de cas (Calcul de table de vérité)


```python
def set_variables (tformule):
    '''
    * La fonction set_variables retourne l'ensemble des variables contenu dans la formule
    '''
    typecheck_formule(tformule)

    tag, val = tformule

    if tag in ('Vrai', 'Faux'):
        return set()

    if tag == 'Var':
        return set([val])

    if tag == 'Non':
        return set_variables(val)

    val1, val2 = val

    if tag in ('Et', 'Ou', 'Implique', 'Equivalent'):
        return set_variables(val1).union(set_variables (val2))
        
    return set()
```


```python
def enumerate_case (set_vars):
    '''
    * La fonction enumerate_case prend en paramètre un ensemble de variables et
      renvoie une liste de dictionnaires.
    * Cette liste représente l'ensemble des combinaisons des valeurs de vérité possible
      pour l'ensemble de variable initial.
    '''
    def aux_enum_case (list_vars):
        if list_vars == []:
            return [{}] 
            
        if len(list_vars) == 1:
            return [{list_vars[0]:True}, {list_vars[0]:False}]

        head, *tail = list_vars
        return list( map( lambda elmnt : {head:True} | elmnt , enumerate_case(tail)) ) + \
            list(map( lambda elmnt : {head:False} | elmnt, enumerate_case(tail)))

    return aux_enum_case(list(set(set_vars)))
```

### Tautologie
Une ```tautologie``` est une formule dont la valeur de vérité est Vraie quelles que soient les valeurs de
vérité de ses variables propositionnelles.


```python
def tautologie(formule):
    """
    * La fonction tautologie génère la liste de dictionnaires
    des valeurs possibles des atomes de la formule formule et
    appelle la fonction tautologie_aux avec cette liste.
    * A chaque appel récursif la fonction tautologie_aux
    évalue formule pour une combinaison de valeurs de
    vérité possibles des atomes de formule.
    * tautologie_aux retourne finalement la conjonction de ces évaluations.
    """
    def tautologie_aux (formule, cas_possible):
        if cas_possible == []:
            return True

        head, *tail = cas_possible
        ret = eval_tf(head, formule) 

        return ret and tautologie_aux(formule, tail)

    typecheck_formule(formule)
    list_dic = enumerate_case(set_variables(formule))
    return tautologie_aux(formule, list(list_dic))

```

### Contradiction
Une formule qui est fausse quelles que soient les valeurs de ses variables propositionnelles est une 
appelée une ```contradiction```.


```python
def contradiction(formule):
    """
    * Une contradiction est une formule qui est fausse quelles que soient les valeurs de ses atomes.
    * La négation d'une contradiction est donc une tautologie.
    * La fonction contradiction utilise donc la fonction tautologie définie précedemment.
    * Si la négation de la formule formule est une tautologie alors formule est une contradiction.
    """
    typecheck_formule(formule)
    return tautologie (('Non', formule))
```

## Algorithme de Shannon
* Pour démontrer qu'une formule est une tautologie, 
il y a une alternative moins coûteuse que la construction
exhaustive de la liste complète des 
cas possible  de la formule (toutes les combinaisons
 possibles de valeurs de vérité des variables propositionnelles de la formule).
 * On va succesivement substituer à chacune des variables
 par une de ses deux valeurs possibles et évaluer la formule.




```python

class SubstError(Exception):
    """
    Exception SubstError pour la fonction substitution 
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)

def substitution (var, valeur, tformule):
    '''
    * La fonction substitution remplace la variable
    var dans la formule tformule par la valeur valeur
    et renvoie la nouvelle formule ainsi créée.
    '''
    typecheck_formule(tformule)

    tag, val = tformule

    if tag in ('Vrai',  'Faux'):
        return tformule

    if tag == 'Var':
        if val == var:
            return valeur
        return tformule

    if tag == 'Non':
        return 'Non', substitution(var, valeur, val)

    val1, val2 = val
    formule = substitution(var, valeur, val1),\
         substitution(var, valeur, val2)
         
    if tag == 'Et':
        return 'Et', formule

    if tag == 'Ou':
        return 'Ou', formule

    if tag == 'Implique':
        return 'Implique', formule

    if tag == 'Equivalent':
        return 'Equivalent', formule
        
    raise SubstError((var, valeur, tformule))
```


```python
def tautologie2(formule):
    """
    * La Fonction tautologie_aux2 évalue la formule par substitution successives
    des variables par leurs valeurs de vérité possibles.
    * C'est la raison pour laquelle à l'appel récursif terminal la liste d'associtation
    est vide. Lors de l'appel à la fonction eval_tf toutes les variables ont été substituées.
    C'est très appréciable car la fonction eval_tf prend pour premier paramètre
    une liste de dictionnaires, or dans tautologie_aux2 list_var est une liste de littéraux
    de variables propositionnelles.    
    """
    def tautologie_aux2(formule, list_var):
        typecheck_formule(formule)
        if list_var == []:
            return eval_tf([], formule)

        head, *tail = list_var
        return tautologie_aux2(substitution(head, ('Vrai', None), formule), tail) and\
            tautologie_aux2(substitution(head, ('Faux', None), formule), tail)

    typecheck_formule(formule)
    return tautologie_aux2(formule, list(set_variables(formule)))
```


```python
def contradiction2(formule):
    """
    * Une contradiction est une formule qui est fausse quelles que soient les valeurs de ses atomes.
    * La négation d'une contradiction est donc une tautologie.
    * La fonction contradiction utilise donc la fonction tautologie2 définie précedemment.
    * Si la négation de la formule formule est une tautologie alors formule est une contradiction.
    """
    typecheck_formule(formule)
    return tautologie2 (('Non', formule))
```
