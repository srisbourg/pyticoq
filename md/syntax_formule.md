# Analyse Syntaxique de formules logiques
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3, p328

Syntaxe concrête du langage de formule


```python
"""
Fonctions d'analyse syntaxique de formule
"""

from ipynb.fs.full.syntax_common import debut_mot, fin_chaine, SyntaxFormuleError, is_keyword, is_ident
```

## Reconnaissance d'une formule
### Reconnaissance de mots clés et d'identificateurs
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3.2.3, p335

La syntaxe concrête est définie en ASCII de la manière suivante :
* ```~``` : pour la ```Négation``` logique
* ```/\``` : pour le ```Et``` logique
* ```\/``` : pour le ```Ou``` logique
* ```==>``` : pour l'```Implication``` logique
* ```<==>``` : pour l'```Equivalence``` logique



```python
def is_var(string, pos):
    """
    La fonction is_var retourne une paire :
    * Une paire représantant une variable dans la syntaxe abstraite créée
      à partir de l'identificateur débutant à la position pos dans la
      chaîne string.
    * La position du prochain mot dans string
    """
    str_v, pos_next = is_ident(string, pos)
    return ('Var', str_v), pos_next
```


```python
def is_lpar(string, pos):
    """
    La fonction is_lpar reconnait une parenthèse ouvrante '('
    """
    return is_keyword('(', string, pos)
```


```python
def is_rpar(string, pos):
    """
    La fonction is_rpar reconnait une parenthèse fermante ')'
    """
    return is_keyword(')', string, pos)
```


```python
def is_and(string, pos):
    """
    La fonction is_and reconnait un ET logique '/\\\\'
    """
    return is_keyword('/\\', string, pos)
```


```python
def is_or(string, pos):
    """
    La fonction is_or reconnait une OU logique '\\\\/'
    """
    return is_keyword('\\/', string, pos)
```


```python
def is_not(string, pos):
    """
    La fonction is_not reconnait la négation '~'
    """
    return is_keyword('~', string, pos)
```


```python
def is_imp(string, pos):
    """
    La fonction is_imp reconnait l'implication '==>'
    """
    return is_keyword('==>', string, pos)
```


```python
def is_equ(string, pos):
    """
    La fonction is_equ reconnait l'implication '<==>'
    """
    return is_keyword('<==>', string, pos)
```


```python
def is_vrai(string, pos):
    """
    La fonction is_vrai reconnait la valeur de vérité 'Vrai'
    """
    try:
        if is_keyword('Vrai', string, pos):
            return ('Vrai', None), debut_mot(string, pos+4)
        raise SyntaxFormuleError
    except SyntaxFormuleError as err:
        raise err    
```


```python
def is_faux(string, pos):
    """
    La fonction is_faux reconnait la valeur de vérité 'Faux'
    """
    try:
        if is_keyword('Faux', string, pos):
            return ('Faux', None), debut_mot(string, pos+4)
        raise SyntaxFormuleError
    except SyntaxFormuleError as err :
        raise err
```


```python
def is_bool(string, pos):
    """
    La fonction is_bool reconnait une valeur de vérité 'Vrai' ou 'Faux'
    """
    try:
        return is_vrai(string, pos)
    except SyntaxFormuleError:
        try :
            return is_faux(string, pos)
        except SyntaxFormuleError as err:
            raise err
```

### Reconnaissance de formule et priorité des opérateurs
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3.2.3, p332

#### Les priorités entres connexteurs sont les suivantes :

```~``` (Non) est prioritaire sur ```/\``` (Et) qui est prioritaire sur ```\/``` (Ou) qui est prioritaire sur ```==>``` (Implique) qui est prioritaire sur ```<==>``` (Equivalent).

* ```~``` (Non) est prioritaire sur ```/\``` (Et) : on lit ```~A /\ B``` comme ```(~A) /\ B``` et non comme ```~(A /\ B)```
* ```/\``` (Et) est prioritaire sur ```\/``` (Ou) : on lit ```A /\ B \/ C``` comme ```(A /\ B) \/ C``` et non comme ```A /\ (B \/ C)```
* ```\/``` (Ou) est prioritaire sur ```==>``` (Implique) : on lit ```A \/ B ==> C``` comme ```(A \/ B) ==> C``` et non comme ```A \/ (B ==> C)```
* ```==>``` (Implique) est prioritaire sur ```<==>``` (Equivalent) : on lit ```A ==> B <==> C``` comme ```(A ==> B) <==> C``` et non comme ```A ==> (B <==> C)```


#### Types de formule
* Les formules de **type 0**  comportent le connecteur ```<==>``` comme connecteur le plus externe 
* Les formules de **type 1**  comportent le connecteur ```==>``` comme connecteur le plus externe
* Les formules de **type 2**  comportent le connecteur ```\/``` comme connecteur le plus externe
* Les formules de **type 3**  comportent le connecteur ```/\``` comme connecteur le plus externe 
* Les formules de **type 4**  comportent le connecteur ```~``` comme connecteur le plus externe 
* Les formules de **type 5**  comportent le connecteur ```Vrai```, ```Faux``` ou des ```parenthèses```

#### Fonctions d'analyse de formule ```is_form0```
Les fonctions d'analyse de formule sont **mutuellement récursives** :
* La fonction ```is_form_par``` construit  **terme parenthésé**, elle utilise la fonction ```is_form0```.
* La fonction ```is_form5``` construit un **terme parenthésé**, une valeur de vérité ```Vrai```
  ou ```Faux``` ou une ```Variable```. Elle utilise les fonctions ```is_form_par```, ```is_bool```
  et ```is_var```.
* La fonction ```is_form4``` construit une **négation** ou un terme généré par la fonction ```is_form5```.
* La fonction ```is_form3``` construit une **conjonction** ou un terme généré par la fonction ```is_form4```.
* La fonction ```is_form2``` construit une **disjontion** ou un terme généré par la fonction ```is_form3```.
* La fonction ```is_form1``` construit une **implication** ou un terme généré par la fonction ```is_form2```.
* La fonction ```is_form0``` construit une **équivalence** ou un terme généré par la fonction ```is_form1```.
* La fonction ```analyse_prop``` appelle la fonction ```is_form0``` pour analyser une chaîne
  et générer un tuple **tformule** de syntaxe abstraite.


```python
def analyse_prop(string):
    """
    * La fonction analyse_prop analyse une chaîne string de syntaxe concrête pour générer une
      tformule de syntaxe abstraite.
    * L'analyse est initiée par un appel à la fonction is_form0 à l'indice 0 de la chaîne.
    * Cet appel doit retourner une tformule formule bien formée et analyser complètement
      les caractères significatifs (appel de la fonction fin_chaine à la position pos).
    * Si ce n'est pas le cas une exception SyntaxFormuleError est levée.
    """
    formule, pos = is_form0(string, debut_mot(string, 0))
    if fin_chaine(string, pos):
        return formule
    raise SyntaxFormuleError(('function analyse_prop', string))

def is_form0(string, pos):
    """
    * La fonction is_form0 analyse une chaîne string à la position pos
      pour détecter le connecteur '<==>'.
    * Elle appelle la fonction is_form1 à la position pos pour déterminer
      le membre gauche (formule1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '<==>' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (formule2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Equivalent', (formule1, formule2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule formule1 et de la position pos1
    """
    formule1, pos1 = is_form1(string, pos)
    try:
        pos2 = is_equ(string, pos1)
        formule2, pos3 = is_form0(string, pos2)
        return ('Equivalent', (formule1, formule2)), pos3
    except SyntaxFormuleError:
        return formule1, pos1

def is_form1(string, pos):
    """
    * La fonction is_form1 analyse une chaîne string à la position pos
      pour détecter le connecteur '==>'.
    * Elle appelle la fonction is_form2 à la position pos pour déterminer
      le membre gauche (formule1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '==>' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (formule2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Implique', (formule1, formule2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule formule1 et de la position pos1
    """
    formule1, pos1 = is_form2(string, pos)
    try:
        pos2 = is_imp(string, pos1)
        formule2, pos3 = is_form1(string, pos2)
        return ('Implique', (formule1, formule2)), pos3
    except SyntaxFormuleError:
        return formule1, pos1


def is_form2(string, pos):
    """
    * La fonction is_form2 analyse une chaîne string à la position pos
      pour détecter le connecteur '\\/'.
    * Elle appelle la fonction is_form3 à la position pos pour déterminer
      le membre gauche (formule1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '\\/' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (formule2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Ou', (formule1, formule2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule formule1 et de la position pos1
    """
    formule1, pos1 = is_form3(string, pos)
    try:
        pos2 = is_or(string, pos1)
        formule2, pos3 = is_form2(string, pos2)
        return ('Ou', (formule1, formule2)), pos3
    except SyntaxFormuleError:
        return formule1, pos1

def is_form3(string, pos):
    """
    * La fonction is_form3 analyse une chaîne string à la position pos
      pour détecter le connecteur '/\\'.
    * Elle appelle la fonction is_form4 à la position pos pour déterminer
      le membre gauche (formule1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '/\\' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (formule2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Et', (formule1, formule2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule formule1 et de la position pos1
    """
    formule1, pos1 = is_form4(string, pos)
    try:
        pos2 = is_and(string, pos1)
        formule2, pos3 = is_form3(string, pos2)
        return ('Et', (formule1, formule2)), pos3
    except SyntaxFormuleError:
        return formule1, pos1

def is_form4(string, pos):
    """
    * La fonction is_form4 analyse une chaîne string à la position pos
      pour détecter le connecteur '~'.
    * Elle essaye de
      - reconnaitre la chaîne '~' à la position pos
      - faire un appel récursif à partir de la position pos1
        pour calculer le membre droit (formule) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Non', formule)
      - la position pos
    * Sinon elle appelle la fonction is_form5 appliquée à la position pos
    """
    try:
        pos1 = is_not(string, pos)
        formule, pos2 = is_form4(string, pos1)
        return ('Non', formule), pos2
    except SyntaxFormuleError:
        return is_form5(string, pos)

def is_form5(string, pos):
    """
    * La fonction is_form5 analyse une chaîne string à la position pos
      pour détecter :
      - Un terme parenthèsé
      - Un littéral 'Vrai' ou 'Faux'
      - Un identifiant de variable
    * Elle appelle la fonction is_form_par pour essayer de reconnaitre
      un terme terme parenthèsé à partir de la position pos.
    * Si cette analyse réussit elle retourne la tformule produite
    * Sinon elle essaye de reconnaitre un littéral 'Vrai' ou 'Faux'
      à partir de la position pos et retourne la tformule ('Vrai', None)
      ou ('Faux', None) correspondante.
    * Sinon elle elle construit une variable de la forme ('Var', '<IDENTIFIANT>')
    """
    try:
        return is_form_par(string, pos)
    except SyntaxFormuleError:
        try:
            return is_bool(string, pos)
        except SyntaxFormuleError:
            return is_var(string, pos)

def is_form_par(string, pos):
    """
    * La fonction is_form_par analyse une chaîne string à la position pos
      pour détecter un terme parenthèsé '( <tformule> )'.
    * On cherche une parenthèse ouvrante '(' à la position pos
    * Par l'appel à la fonction is_form0 on cherche le terme formule mis
      entre parenthèse à la position pos1
    * On cherche finalement la parenthèse fermante ')' à la position pos2
    * On retourne la paire constituée de :
      - la tformule formule
      - la position pos3
    """
    pos1 = is_lpar(string, pos)
    formule, pos2 = is_form0(string, pos1)
    pos3 = is_rpar(string, pos2)
    return formule, pos3
```
