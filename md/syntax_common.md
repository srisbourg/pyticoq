```python
"""
Fonctions d'analyse syntaxique communes aux deux langages
"""
```


```python
def ignore_char(char):
    """
    * La fonction ignore_char retourne True si le caractère char
    appartient à la liste [' ', '\\n', '\\t'] et False sinon.
    """
    if char in [' ', '\n', '\t'] :
        return True
    return False
```


```python
def debut_mot(string, pos):
    """
    La fonction debut_mot retourne pour une chaîne string
    et une position pos  dans la chaine, 
    la position du premier caractère 
    significatif (que l'on n'ignore pas).
    """
    i = pos
    while i < len(string) and ignore_char(string[i]):
        i += 1
    return i
```


```python
def fin_chaine(string, pos):
    """
    * La fonction fin_chaine teste dans la chaîne string 
    si les caractères suivants le caractère à la position pos
    sont non significatifs.
    * Exemple : Pour string = "jdfs    " les caractères après le string sont
    non significatifs.
    """
    return debut_mot(string, pos) == len(string)
```


```python
class SyntaxFormuleError(Exception):
    """
    Exception SyntaxFormuleError pour les fonctions d'analyse syntaxique de formule
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)
```


```python
def is_keyword(keyword, string, pos, exceptio=SyntaxFormuleError):
    """
    * La fonction is_keyword reconnait un mot clé du langage comme
      une sous-chaîne  de la chaîne string à la position pos. 
    * Elle retourne la position du mot suivant
    """
    kw_length = len(keyword)
    if string[pos:pos+kw_length] == keyword:
        return debut_mot (string, pos+kw_length)
    raise exceptio(("function is_keyword", keyword, string, pos))    
```


```python
def possible(char):
    """
    * La fonction possible teste si le caractère char est :
      - soit un caractère alphabétique
      - soit un caractère numérique
      - soit underscore '_'
    * Elle renvoie True si cest le cas et False sinon
    """
    return char.isalnum() or char == '_' 
```


```python
def is_ident(string, pos, exceptio=SyntaxFormuleError):
    """
    * La fonction is_ident retourne une paire :
      - un identificateur présent dans la chaîne string 
      commençant à la position pos.
      - la position du prochain mot dans string
    * L'identificateur est une sous-chaîne de string
    * Si la sous-chaîne contient un caractère interdit ou que
    la paramètre pos est inadéquat la fonction lève une exception exceptio.
    """
    if pos == len(string) or not possible(string[pos]):
        raise exceptio(("function is_ident", (string, pos)))
    j = 1
    while pos + j < len(string) and possible(string[pos+j]):
        j += 1
    len_word = j
    while pos + j < len(string) and ignore_char(string[pos+j]):
        j += 1
    return string[pos:pos+len_word], pos + j
```
