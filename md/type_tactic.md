# Syntaxe abstraite du langage de tactiques
Apprentissage de la programmation en OCaml de C. Dubois et al (2004), §16.2.2, p348

Un deuxième langage est nécessaire pour réaliser l'assistant de preuve.
Nous utiliserons la même méthodologie pour l'analyser et l'interpréter.

* Il est plus simple que le premier, nous de définissons pas un type réccursif
* Sa grammaire BNF est définie comme suit:<BR/>
Tactique := <BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```split```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```left```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```right```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```exact<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```decompose<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```case<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```intro<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```apply<string>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```absurd<Formule>```<BR/>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   | ```intros<List string>```
* On remarque par contre que le constructeur ```absurd``` est construit à l'aide du type ```Formule```


```python
"""
Module de vérification de tuple du langage de tactique
"""

from ipynb.fs.full.type_formule import TypeCheckFormuleError, typecheck_formule
```


```python
def is_ttactic (tactic):
    '''
    * La fonction is_ttactic vérifie le type de son paramètre tactic.
    * Si tactic est un tuple respectant la grammaire BNF des tactiques la 
    fonction renvoie True sinon elle renvoie False.
    * Ce tuple est constitué d'un tag de type chaîne de caractère et d'une Valeur.
      + Les tag peuvent être 'split'|'left'|'right'|'exact'|'decompose'|'case'|'intro'|'apply'|'absurd'|'intros'
      + Les valeurs peuvent être :
        - None, pour les tags 'split', 'left' et 'right'
        - Une chaîne de caractères pour les tags 'exact', 'decompose', 'case', 'intro' et 'apply'
        - Une formule pour le tag 'absurd'
        - Une liste de chaînes de caractères pour le tag 'intros'
    * La fonction renvoie False si la structure est incohérente
    * La fonction renvoie True si la structure est valide.
    * Une exception ValueError est levée si l'argument tactic n'est pas une paire
    '''
    try:
        tag, val = tactic
    except ValueError as err:
        raise err

    if tag in ('split', 'left', 'right') and val is None:
        return True
    
    if tag in ('exact', 'decompose', 'case', 'intro', 'apply') and isinstance(val, str):
        return True

    if tag == 'absurd':
        try:
            typecheck_formule(val)
            return True
        except TypeCheckFormuleError as err:
            raise err

    if tag == 'intros':
        if isinstance(val, list) and len(val) > 0:
            for vval in val:
                if not isinstance(vval, str):
                    return False
            return True
    
    return False
```


```python
class TypeCheckTacticError(Exception):
    """
    Exception pour la fonction typecheck_tactic
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

def typecheck_tactic(tact):
    """
    * La fonction typecheck_tactic vérifie que son paramètre tact est
      un tuple bien structuré respectant la grammaire des 
      tactiques.
    * Si ce n'est pas le cas elle lève une exception TypeCheckTacticError
    """
    try:
        if not is_ttactic(tact):
            raise TypeCheckTacticError(tact)
    except ValueError as err:
        raise TypeCheckTacticError(tact) from err
    except TypeCheckFormuleError as err:
        raise TypeCheckTacticError(tact) from err
```
