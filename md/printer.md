# Fonctions d'affichage


```python
"""
Fonctions d'affichage
"""

from ipynb.fs.full.type_formule import typecheck_formule
from ipynb.fs.full.type_context import typecheck_context
from ipynb.fs.full.type_goal import typecheck_goal
from ipynb.fs.full.type_tactic import typecheck_tactic
```


```python
def formule_priority(tformule):
    """
    * La priorité  d'une formule est représentée par un
    un entier entre 1 et 6.
    * Une formule de type 0 (<==>) est de priorité 1
    * Une formule de type 1 (==>) est de priorité 2
    * Une formule de type 2 (\\/) est de priorité 3
    * Une formule de type 3 (/\\) est de priorité 4
    * Une formule de type 4 (~) est de priorité 5
    * Une formule de type 5 (variable ou
      valeur de vérité) est de priorité 6
    """
    typecheck_formule(tformule)
    
    tag, _ = tformule
    
    if tag == 'Equivalent':
        return 1
    
    if tag == 'Implique':
        return 2
    
    if tag == 'Ou':
        return 3
    
    if tag == 'Et':
        return 4
    
    if tag == 'Non':
        return 5
    
    return 6
```


```python

class StringFormuleError(Exception):
    """
    Exception TFormuleError pour la fonction is_tformule 
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

def string_of_formule(tformule):
    """
    * La fonction string_of_formule prend en paramètre une formule 
      dans sa représentation en syntaxe abstraite et la traduit
      en une chaîne de caractères de syntaxe concrête.
    """
    typecheck_formule(tformule)

    priority = formule_priority(tformule)
    
    tag, val = tformule
    
    if tag in ('Vrai', 'Faux'):
        return tag

    if tag == 'Var':
        return val
        
    if tag == 'Non':
        return '~' + parent_string_of_formule(val, priority, False)
    
    try :
        val1, val2 = val
    except TypeError as err:
        raise StringFormuleError(tformule) from err

    sv1 = parent_string_of_formule(val1, priority, True)
    sv2 = parent_string_of_formule(val2, priority, False)
    
    if tag == 'Et':
        return  sv1 + ' /\\ ' + sv2

    if tag == 'Ou':
        return sv1 + ' \\/ ' + sv2

    if tag == 'Implique':
        return sv1 + ' ==> ' + sv2

    if tag == 'Equivalent':
        return sv1 + ' <==> ' + sv2
    
    raise StringFormuleError(tformule)


def parent_string_of_formule(formule, priority, assoc):
    """
    En fonction de la priorité de la sous-formule formule par rapport à la formule parente
    et de l'associativité à gauche la formule est écrite avec ou sans parenthèses.
    """
    typecheck_formule(formule)

    if formule_priority(formule) < priority or (formule_priority(formule) == priority and assoc):
        return '(' + string_of_formule(formule) + ')'
    return string_of_formule(formule)
```


```python
def print_formule(tformule):
    """
    La fonction print_formule prend en paramètre une formule 
    en syntaxe abstraite et l'affiche en syntaxe concrête.
    """
    typecheck_formule(tformule)
    print(string_of_formule(tformule))
```


```python
def string_of_hyp (ident, formule):
    """
    * La Fonction string_of_hyp prend en paramètre 
      un identifiant et une formule du contexte et
      retourne une chaîne de caractères de la forme :
      'identifiant : formule'
    """
    typecheck_formule(formule)
    return ident + ' : ' + string_of_formule(formule)
```


```python
def string_of_context(context):
    """
    La fonction string_of_context prend en paramètre un contexte
    et retourne une chaine de caractères de la forme :
    'id0 : form0\\nid1 : form1\\nid2 : form2\\nid3 : form3'
    """
    typecheck_context(context)
    ret = ''
    for _id in context.keys():
        ret += string_of_hyp(_id, context[_id]) + '\n'
    return ret
```


```python
def string_of_goal(goal):
    """
    La fonction string_of goal prend en paramètre un but à prouver (goal)
    et retourne une chaîne de caractère de la la forme :
    h0 : form0
    h1 : form1
    ============
    formGoal
    """
    typecheck_goal(goal)
    longbar = '=' * 50
    return '\n' + string_of_context(goal['context']) +\
        longbar + '\n' + string_of_formule(goal['formule']) + '\n'
```


```python
def fresh_ident(prefix='_hyp'):
    """
    Le générateur fresh_ident produit des chaîne de
    caractères de la forme :
    'prefix0', 'prefix1', 'prefix2', 'prefix3', ...
    """
    num = 0
    while True:
        yield prefix + str(num)
        num += 1
```


```python
class StringTacticError(Exception):
    """
    Exception StringTacticError pour la fonction string_of_tactic
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

def string_of_tactic(tactic):
    """
    * La fonction string_of_tactic prend en paramètre une tactique
      dans sa représentation en syntaxe abstraite et la traduit
      en une chaîne de caractères de syntaxe concrête.
    """
    typecheck_tactic(tactic)
    try :
        tag, val = tactic
    except TypeError as err:
        raise StringTacticError(tactic) from err

    if val is None:
        return tag

    if isinstance(val, str):
        return tag + ' ' + val

    if tag == 'absurd':
        typecheck_formule(val)
        return tag + ' ' + string_of_formule(val)    
            
    if tag == 'intros':
        ret=''
        for i in range(len(val)-1): # Gestion de la dernière virgule
            ret += val[i] + ', '
        return tag + ' ' + ret + val[len(val)-1]

    raise StringTacticError(tactic)
```
