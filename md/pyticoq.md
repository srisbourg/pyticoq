# PytiCoq


```python
"""
Fonctions du prouveur interactif
"""

import argparse
from ipynb.fs.full.syntax_formule import analyse_prop
from ipynb.fs.full.type_formule import typecheck_formule
from ipynb.fs.full.type_context import typecheck_context, valid_ident
from ipynb.fs.full.type_goal import typecheck_goal, mk_goal
from ipynb.fs.full.type_tactic import typecheck_tactic
from ipynb.fs.full.syntax_tactic import analyse_tact, EndException, SyntaxTacticError
from ipynb.fs.full.printer import string_of_formule, fresh_ident, string_of_tactic, string_of_goal

```


```python
class IdentInvalidError(Exception):
    """
    Exception IdentInvalidError
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

class TacticError(Exception):
    """
    Exception TacticError
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)
```


```python
def introduction (hyps, context, formule):
    """
    * La fonction introduction prend en paramètre :
      - La liste hyps des noms des hypothèses que l'on va ajouter au contexte
      - Le contexte context
      - La formule 'formule' à prouver
    * Si la formule est une implication de la forme :
      f0 ==> f1 ==> f2 ==> ... ==> fn ==> f'
    * Si les identifiants contenuent dans hyps sont valides et de la forme :
      h0, h1, h2, ..., hm (m <= n)
    * Alors la fonction retourne une paire composée
      - D'un nouveau contexte tel que :
        new_context = {h0:f0, h1:f1, h2:f2, ..., hm:fm} | context 
      - D'une formule tel que :
        fm+1 ==> ... ==> fn ==> f'
    * La fonction s'applique récursivement sur la liste des hypothèses
    * Si le contexte contient déjà une hypothèse dont l'identifiant
      est le même qu'un de ceux contenus dans hyps une exception
      IdentInvalidError est lévée
    * Si la formule n'est pas une implication une exception TacticError
      est levée.
    """
    typecheck_formule(formule)
    typecheck_context(context)

    if hyps == []:
        return context, formule

    tag, val = formule

    if tag == 'Implique':
        head, *tail = hyps
        if valid_ident(head, context):
            formule1, formule2 = val
            new_context = context.copy()
            new_context[head] = formule1
            return introduction(tail, new_context, formule2)
        raise IdentInvalidError((head, hyps, context, formule))

    raise TacticError(('function introduction', hyps, context, formule))    
```


```python
def conditions_apply(phi, formule):
    """
    * La fonction conditions_apply prend en paramètre une formule
      phi et une formule 'formule'
    * Si phi est de la forme : f1 ==> (f2 ==> ... (fn ==> formule))
      la fonction conditions_apply produit la liste de conditions
      f1, f2, ... , fn
    * Si la fonction échoue elle lève une exception TacticError
    """
    typecheck_formule(phi)
    typecheck_formule(formule)

    tag, val = phi
    if tag == 'Implique':
        formule1, formule2 = val
        if formule2 == formule:
            return [formule1]
        return [formule1] + conditions_apply(formule2, formule)

    raise TacticError(("function conditions_apply", phi, formule))

```


```python
def apply_tactic(tactic, goal, gen_fresh_ident):
    """
    * La fonction apply_tactic est la plus interessante.
    * Elle prend en paramètre :
      - Un tuple de syntaxe abstraite de tactique
      - Un but
      - Un générateur d'identifiants d'hypothèses frais
    * Si c'est possible, elle applique au but la tactique selon 
      les règles de la déduction naturelle.
    * Il y a une vraie correspondance entre le retour de la fonction 
      quand une tactique s'applique et les règles d'introduction et d'élimination
      décrites dans le README.
    * Elle retourne une liste modifiée de buts.  
    """
    typecheck_tactic(tactic)
    typecheck_goal(goal)
        
    # Tactique
    tagt, valt = tactic

    # Formule
    formule = goal['formule']
    tagf, valf = formule

    # Contexte
    ctx = goal['context']
    
    # Cas Tactique exact
    if tagt == 'exact':
        if valt in ctx.keys():
            if ctx[valt] == formule:
                return []
        raise IdentInvalidError(('function apply_tactic exact : ' +\
             valt + ' pas dans le contexte', tactic, goal))
            
    # Cas Tactique split
    if tagt == 'split':
        formule1, formule2 = valf
        if tagf == 'Et':            
            return [mk_goal(ctx, formule1), mk_goal(ctx, formule2)]
        if tagf == 'Equivalent':
            return [mk_goal(ctx, ('Implique', (formule1, formule2))),\
                    mk_goal(ctx, ('Implique', (formule2, formule1)))]
   
    # Cas Tactique case
    if tagt == 'case':
        if valt in ctx.keys():
            tag, val =  ctx[valt]
            if tag == 'Ou':
                phi1, phi2 = val
                num1 = next(gen_fresh_ident)
                num2 = next(gen_fresh_ident)
                return [mk_goal( {num1:phi1} | ctx, formule),\
                    mk_goal( {num2:phi2} | ctx, formule)]
        
        raise IdentInvalidError(('function apply_tactic case : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique decompose    
    if tagt == 'decompose':                
        if valt in ctx.keys() :
            tag, val =  ctx[valt]
            if tag == 'Et':
                phi1, phi2 = val
                num1 = next(gen_fresh_ident)
                num2 = next(gen_fresh_ident)
                return [mk_goal({num1:phi1, num2:phi2} | ctx, formule)]
        
        raise IdentInvalidError(('function apply_tactic decompose : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique left                                    
    if tagt == 'left' and tagf == 'Ou':
        formule1, _ = valf
        return [mk_goal(ctx, formule1)]

    # Cas Tactique right
    if tagt == 'right' and tagf == 'Ou':
        _, formule2 = valf
        return [mk_goal(ctx, formule2)]
        
    # Cas Tactique intro
    if tagt == 'intro' and tagf == 'Implique':
        if valid_ident(valt, ctx):
            formule1, formule2 = valf
            return [mk_goal({valt:formule1} | ctx, formule2)]
        
        raise IdentInvalidError(('function apply_tactic intro : '+\
             valt + ' existe dans le contexte', tactic, goal))

    # Cas Tactique intros
    if tagt == 'intros':
        if tagf == 'Implique':
            try:
                nv_c, phi = introduction(valt, ctx, formule)
                return [mk_goal(nv_c, phi)]
            except TacticError as err:
                raise err

    # Cas Tactique apply    
    if tagt == 'apply':
        if valt in ctx.keys():
            phi =  ctx[valt]
            try:
                return list(map(lambda ff : mk_goal(ctx, ff),\
                    conditions_apply(phi, formule)))
            except TacticError as err:
                raise err
        
        raise IdentInvalidError(('function apply_tactic apply : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique absurd
    if tagt == 'absurd':
        return [mk_goal(ctx, valt), mk_goal(ctx, ('Non', valt))]
    
    # Cas Tactique invalide
    raise TacticError(('function apply_tactic Invalid Tactic', tactic, goal))
```


```python
def step(goal, gen_fresh_ident, strtact=None):
    """
    * La fonction step applique une tactique strtact sur le but goal
    * Si le paramètre strtact vaut None la démonstration est intéractive
      et demande la saisie d'une tactique.
    """
    typecheck_goal(goal)
    auto = strtact is not None

    try:    
        if auto:            
            tac = analyse_tact(strtact)
        else:
            tac = analyse_tact(input('Saisie tactique : ').strip()) 

    except SyntaxTacticError as err:
        if auto:
            raise err
        print('Erreur de Syntaxe')
        freshi = fresh_ident()
        return step(goal, freshi)

    except EndException as err:
        raise err

    typecheck_tactic(tac)       
    str_of_tac = string_of_tactic(tac)

    try:
        ret = apply_tactic(tac, goal, gen_fresh_ident) 

        if auto:
            print(str_of_tac)
            
        return ret

    except TacticError as err:
        if auto:
            raise err
        print('Tactique Invalide')
        freshi = fresh_ident()
        return step(goal, freshi)  

    except IdentInvalidError as err:
        if auto:
            raise err
        print('Identifiant Invalide')
        freshi = fresh_ident()
        return step(goal, freshi) 

    except err:
        if auto:
            raise err from err
        print('Erreur indéfinie')
        freshi = fresh_ident()
        return step(goal, freshi)
        

```


```python
def interactive_loop(goals, arrstrtactics=None):
    """
      * La fonction interactive_loop gère la liste des buts à prouver
      * Elle prend en paramètre une liste de buts et optionnellement un tableau
        de chaine de caractères de syntaxe concrête de tactiques.
      * Si la liste de buts est vide la preuve est terminée et la fonction retourne 0
      * Sinon la fonction affiche le but courant
      * En mode automatique, la fonction essaye d'appliquer la tactique en tête de liste
      * Sinon le but doit être prouvé en mode interactif
    """
    auto = arrstrtactics is not None
    
    for goal in goals:
        typecheck_goal(goal)

    if goals == []:
        return 0

    current_goal, *tail_goals = goals
    print(string_of_goal(current_goal))

    freshi = fresh_ident()

    if auto:
        tact, *tail_tact = arrstrtactics
        new_goals = step(current_goal, freshi, tact) + tail_goals
        return interactive_loop(new_goals, tail_tact)

    new_goals = step(current_goal, freshi) + tail_goals
    return interactive_loop(new_goals)      
```


```python
def pyticoq(strformule = None, tactics = None):
    """
        * La fonction pyticoq prend en paramètre :
          - une chaine de caractères de syntaxe concrète de formule
          - Un tableau de chaine de caractères de syntaxe concrête de tactiques
        * Si la fonction est appelée sans paramètre :
          L'utilisateur doit saisir la formule à prouver et faire la preuve en mode interactif
        * Si le paramètre  strformule est donné :
          L'assistant se lance alors directement pour débuter la preuve de la formule strformule.
        * Si les paramètres  strformule et  tacttics sont donnés : L'assistant essaye de prouver 
          la formule avec la séquence de tactiques.
    """
    if not strformule:
        try:
            formule = analyse_prop(input('Saisie Formule ? ').strip())
            print('Formule à prouver : ' + string_of_formule(formule) + '\n\n' + ('%' * 50) + '\n')
            interactive_loop([mk_goal({}, formule)])
            print('\nQed')

        except EndException:
            print('Bye Bye')
            

    elif not tactics:
        try:
            formule = analyse_prop(strformule.strip())
            print(string_of_formule(formule))
            interactive_loop([mk_goal({}, formule)])
            print('\nQed')

        except EndException:
            print('Bye Bye')
            

    else:
        try:
            formule = analyse_prop(strformule.strip())
            interactive_loop( [mk_goal({}, formule) ], tactics)
            print('\nQed')

        except EndException :
            print('Bye Bye')
            

```

  
        * Point d'entrée principal du programme.
        * L'outil peut être appelé :
          - Sans paramètre : L'utilisateur doit saisir la formule à prouver
            et faire la preuve en mode interactif
          - Avec le paramètre '--formule' et une formule sous forme de chaine de caractères.
            L'assistant se lance alors directement pour débuter la preuve de la formule.
          - Avec le paramètre '--formule', une formule et le paramètre '--tactics' suivi
            d'une chaine de caractères décrivant une séquence de tactiques séparées par
            des points virgule.


```python
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pitycoq')
    parser.add_argument('--formule', help='formule help')
    parser.add_argument('--tactics', help='tactics help')
    args = parser.parse_args()

    if not args.formule:
        pyticoq()
    
    elif not args.tactics:
        pyticoq(args.formule.strip())
    
    else:
        tactics_array = list(map(lambda x : x.strip(),\
            args.tactics.split(';')))

        pyticoq(args.formule.strip(), tactics_array)
    
```
