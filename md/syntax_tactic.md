# Analyse syntaxique d'une tactique
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.5.5, p357

Syntaxe concrête du langage de tactique


```python
"""
Module pour l'analyse syntaxique du langage de tactiques
"""
from ipynb.fs.full.syntax_common import debut_mot, fin_chaine, is_keyword, is_ident
from ipynb.fs.full.syntax_formule import SyntaxFormuleError, is_form0
```


```python
class SyntaxTacticError(Exception):
    """
    Exception pour les erreurs de syntaxe du langage de tactiques
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)
```


```python
def is_coma(string, pos):
    """
    La fonction is_coma reconnait une virgule ','
    """
    return is_keyword(',', string, pos, SyntaxTacticError)
```


```python


def is_hyp_aux(string, pos):
    """
    * La fonction is_hyp_aux détecte un identiant d'hypothèse
    * Si cette identifiant commence par un underscore '_' une exception
      SyntaxTacticError est levée
    * Sinon la fonction retourne la paire composée de  l'identifiant et de la position du mot suivant
    """
    idt, pos1 = is_ident(string, pos, SyntaxTacticError)
    if idt[0] == '_':
        raise SyntaxTacticError(('is_hyp_aux', string, pos))
    return idt, pos1
```


```python
def is_hyp(string, pos):
    """
    * La fonction is_hyp reconnait des identifiants d'hypothèses séparées par des virgules.
    * Si l'hypothèse est unique la fonction retourne une liste contenant pour unique élément hyp
    * Sinon elle construit récursivement la liste de toutes les hypothèses et la retourne.
    """
    hyp, pos1 = is_hyp_aux(string, pos)
    try:
        pos2 = is_coma(string, pos1)
    except SyntaxTacticError :
        return [hyp], pos1
    harr, pos3 = is_hyp(string, pos2)
    return [hyp] + harr, pos3
    
```


```python
class EndException(Exception):
    """
    Exception EndException pour interrompre le prouveur interactif
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)


def is_tact(string, pos):
    """
    * La fonction is_tact reconnait les mots clé du langage de tactiques
      dans une chaine de caractères string et construit le tuple de syntaxe abstraite
      correspondant.
    * La tactique quit lance une exception EndException
    * Si l'analyse syntaxtique détecte un mot clé non autorisé une
      exception SyntaxTacticError est levée
    """
    idt, pos1 = is_ident(string, pos, SyntaxTacticError)
    if idt in ('split', 'left', 'right'):
          if fin_chaine(string, pos1):
              return ((idt, None), pos1)

    if idt == 'intro':
        hyp, pos2 = is_hyp_aux(string, pos1)
        return ((idt, hyp), pos2)
    
    if idt in ('exact', 'case', 'decompose', 'apply'):
        hyp, pos2 = is_ident(string, pos1, SyntaxTacticError)
        return ((idt, hyp), pos2)
    
    if idt == 'intros':
        hyp, pos2 = is_hyp(string, pos1)
        return ((idt, hyp), pos2)
    
    if idt == 'absurd':
        try:
            formule, pos2 = is_form0(string, pos1)
            return ((idt, formule), pos2)
        except SyntaxFormuleError as err:
            raise err
        
    if idt == 'quit':
        raise EndException("quit")
        
    raise SyntaxTacticError(('is_tact', string, pos))
```


```python
def analyse_tact(string):
    """
    * La fonction analyse_tact tente de reconnaitre une tactique dans la chaine
      de caractères string:
      - Si la chaine est bien formée, la fonction retourne un tuple du langage de tactique
      - Sinon elle lève une exception SyntaxTacticError
    """
    tact, pos = is_tact(string, debut_mot(string, 0))
    if fin_chaine(string, pos):
        return tact
    raise SyntaxTacticError(('analyse_tact', string))
```


```python

```
