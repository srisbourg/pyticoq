# Contexte d'une Preuve


```python
"""
Module de définition et de vérification du type context
"""

from ipynb.fs.full.type_formule import TypeCheckFormuleError, is_tformule

```


```python
def is_tcontext(context):
    """
    * La fonction is_tcontext vérifie si le paramètre tcontext
    est bien un dictionnaire dont:
      - Les clés sont des chaînes de caractères
      - Les valeurs sont des formules
    * Elle retourne True si l'argument est bien structuré
    * Sinon elle retourne False
    """
    if not isinstance(context, dict) :
        return False
    for k in context.keys():
        try:
            is_tformule(context[k])
        except TypeCheckFormuleError as err:
            raise err
        if not isinstance(k, str) :
            return False
    return True
```


```python


class TContextError(Exception):
    """
    Exception TypeCheckFormuleError pour la fonction typecheck_context
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

def typecheck_context(ctx):
    """
    * La fonction typecheck_context vérifie si le 
      paramètre ctx est bien un contexte.
    * Elle lance une exception TContextError si ce n'est
      pas le cas.
    """
    try:
        if not is_tcontext(ctx):
            raise TContextError(ctx)
    except TypeCheckFormuleError as err:
        raise TContextError(ctx) from err
```


```python
def valid_ident(ident, context):
    """
    * La fonction valid_ident vérifie que l'identifiant
      candidat ident n'est pas présent comme clé 
      dans le contexte context.
    * Si le contexte contient déjà une clé de même 
      valeur que le paramètre ident la fonction retourne
      False.
    * Sinon la fonction retourne True.
    """
    typecheck_context(context)
    for k in context.keys():
        if k == ident:
            return False
    return True   
```
