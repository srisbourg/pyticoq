import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_formule import *
from unit_test_common import *
from syntax_formule import *

debut_test_module('type_formule')

# Tests type_formule.is_tformule
debut_test_fonction('is_tformule')
print(is_tformule.__doc__)

istf_count = gen_int_count()

test_exception(next(istf_count), lambda x : is_tformule(('Et', 'Faux')),\
    'is_tformule', 'Tuple Invalide')

tf_struct_incoherente = 'Coucou', ('Faux', 'Dodo')
test_ifnot(next(istf_count), is_tformule(tf_struct_incoherente), 'is_tformule',\
    'Formule incohérente : ')



fin_test_fonction('is_tformule')

# Tests type_formule.typecheck_formule
debut_test_fonction('typecheck_formule')
print(typecheck_formule.__doc__)
typecheck_formule_count = gen_int_count()

test_exception(next(typecheck_formule_count), lambda x : typecheck_formule(('Et', 'Faux')),\
    'is_tformule', 'Tuple Invalide')

test_exception(next(typecheck_formule_count), lambda x : typecheck_formule(('Coucou', ('Faux', 'Dodo'))),\
    'is_tformule', 'Tuple Invalide')

test_exception(next(typecheck_formule_count), lambda x : typecheck_formule(('Vrai', ('Faux', 'Dodo'))),\
    'is_tformule', 'Formule incohérente')


fin_test_fonction('typecheck_formule')

fin_test_module('type_formule')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')