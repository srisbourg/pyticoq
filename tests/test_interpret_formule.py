import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from interpret_formule import *
from unit_test_common import *

# DONE : !!! NE PLUS MODIFIER !!!

debut_test_module('interpret_formule')

# Tests interpret_formule.assoc
debut_test_fonction('assoc')
print(assoc.__doc__)

ass_count = gen_int_count()

test_if(next(ass_count), assoc('P', {'P': True}), 'assoc', "Proposition True dans le dictionnaire")

test_ifnot(next(ass_count), assoc('P', {'P': False}), 'assoc', "Proposition False dans le dictionnaire")

test_exception(next(ass_count), lambda x : assoc('T', {'P': True}), 'assoc', "Proposition absente du dictionnaire")

test_exception(next(ass_count), lambda x : assoc('P', {'P': 42}), 'assoc', "Valeur invalide dans le dictionnaire")

fin_test_fonction('assoc')

# Tests interpret_formule.eval_tf
debut_test_fonction('eval_tf')
print(eval_tf.__doc__)

eval_count = gen_int_count()

la = {'P':True, 'Q':False}
tf = 'Equivalent', (('Implique', (('Var', 'P'), ('Var', 'Q'))),\
                        ('Ou', (('Non', ('Var', 'P')), ('Var', 'Q'))))

test_if(next(eval_count), eval_tf(la, tf), 'eval_tf', "Evaluation d'une proposition")

la2 = {'P':True}

test_exception(next(eval_count), lambda x : eval_tf(la2, tf), 'eval_tf', "Liste d'association incomplète")

tf2 = 'Vrai', ('Faux', 'Dodo')

test_exception(next(eval_count), lambda x : eval_tf(la2, tf2), 'eval_tf', "Formule invalide")

fin_test_fonction('eval_tf')

# Tests interpret_formule.set_variables
debut_test_fonction('set_variables')
print(set_variables.__doc__)

vars_count = gen_int_count()

test_if(next(vars_count), set_variables(tf) == set(['P', 'Q']), 'set_variables', "Ensemble des variables d'une formule")

fin_test_fonction('set_variables')

# Tests interpret_formule.enumerate_case
debut_test_fonction('enumerate_case')
print(enumerate_case.__doc__)

enum_count = gen_int_count()

oracle = [{'R': True, 'P': True, 'Q': True},
 {'R': True, 'P': True, 'Q': False},
 {'R': True, 'P': False, 'Q': True},
 {'R': True, 'P': False, 'Q': False},
 {'R': False, 'P': True, 'Q': True},
 {'R': False, 'P': True, 'Q': False},
 {'R': False, 'P': False, 'Q': True},
 {'R': False, 'P': False, 'Q': False}]

test_compare_list(next(enum_count), 'enumerate_case',\
     "Ensemble des combinaisons de valeurs possible pour un ensemble de variables",\
         oracle, enumerate_case(set(['P', 'Q', 'R'])))

fin_test_fonction('enumerate_case')

# Tests interpret_formule.tautologie
debut_test_fonction('tautologie')
print(tautologie.__doc__)
taut_count = gen_int_count()

test_if(next(taut_count), tautologie(tf), 'tautologie', "Loi de Morgan : (A => B) <=> (~A \/ B)")

f0 = 'Non', ('Ou', (('Var', 'A'),('Var', 'B')))
f1 = 'Et', (('Non', ('Var', 'A')),('Non', ('Var', 'B')))
f = ('Equivalent', (f0, f1))
test_if(next(taut_count), tautologie(f), 'tautologie', "Loi de Morgan : ~(A \/ B) <=> (~A /\ ~B)")

f0 = 'Non', ('Et', (('Var', 'A'),('Var', 'B')))
f1 = 'Ou', (('Non', ('Var', 'A')),('Non', ('Var', 'B')))
f = ('Equivalent', (f0, f1))
test_if(next(taut_count), tautologie(f), 'tautologie', "Loi de Morgan : ~(A /\ B) <=> (~A \/ ~B)")

test_if(next(taut_count), tautologie(('Implique', (('Var', 'A'),('Var', 'A')))), 'tautologie', "A implique A")
test_if(next(taut_count), tautologie(('Equivalent', (('Var', 'A'),('Var', 'A')))), 'tautologie', "A équivalent à A")

test_if(next(taut_count), tautologie(('Vrai', None)), 'tautologie', "Vrai")

test_if(next(taut_count), tautologie(('Non', ('Faux', None))), 'tautologie', "Non Faux")

f1 = 'Et', (('Non', ('Var', 'A')), ('Var', 'A'))
test_if(next(taut_count), tautologie(('Non', f1 )), 'tautologie', "~(A /\ ~A)")

fin_test_fonction('tautologie')

# Tests interpret_formule.contradiction
debut_test_fonction('contradiction')
print(contradiction.__doc__)
cont_count = gen_int_count()

test_if(next(cont_count), contradiction(('Et', (('Var', 'P'),('Non', ('Var', 'P'))))), 'contradiction', "P /\ ~P")

fin_test_fonction('contradiction')

# Tests interpret_formule.substitution
debut_test_fonction('substitution')
print(substitution.__doc__)
substi_count = gen_int_count()

f1 = ('Et', (('Var', 'P'),('Non', ('Var', 'P'))))
f2 = ('Et', (('Vrai', None), ('Non', ('Vrai', None))))
f3 = ('Et', (('Faux', None), ('Non', ('Faux', None))))

test_if(next(substi_count), substitution('P', ('Vrai', None), f1) == f2, 'substitution', "P /\ ~P devient Vrai /\ ~Vrai")
test_if(next(substi_count), substitution('P', ('Faux', None), f1) == f3, 'substitution', "P /\ ~P devient Faux /\ ~Faux")

f4 = ('Equivalent', (('Implique', (('Var', 'P'), ('Var', 'Q'))), ('Ou', (('Non', ('Var', 'P')), ('Var', 'Q')))))
f5 = ('Equivalent', (('Implique', (('Vrai', None), ('Var', 'Q'))), ('Ou', (('Non', ('Vrai', None)), ('Var', 'Q')))))

test_if(next(substi_count), substitution('P', ('Vrai', None), f4) == f5, 'substitution', "P => Q <=> P \/ ~Q devient Vrai => Q <=> Vrai \/ Q")

fin_test_fonction('substitution')

# Tests interpret_formule.tautologie2
debut_test_fonction('tautologie2')
print(tautologie2.__doc__)
taut2_count = gen_int_count()

test_if(next(taut2_count), tautologie2(tf), 'tautologie2', "Loi de Morgan : (A => B) <=> (~A \/ B)")

f0 = 'Non', ('Ou', (('Var', 'A'),('Var', 'B')))
f1 = 'Et', (('Non', ('Var', 'A')),('Non', ('Var', 'B')))
f = ('Equivalent', (f0, f1))
test_if(next(taut2_count), tautologie2(f), 'tautologie2', "Loi de Morgan : ~(A \/ B) <=> (~A /\ ~B)")

f0 = 'Non', ('Et', (('Var', 'A'),('Var', 'B')))
f1 = 'Ou', (('Non', ('Var', 'A')),('Non', ('Var', 'B')))
f = ('Equivalent', (f0, f1))
test_if(next(taut2_count), tautologie2(f), 'tautologie2', "Loi de Morgan : ~(A /\ B) <=> (~A \/ ~B)")

test_if(next(taut2_count), tautologie2(('Implique', (('Var', 'A'),('Var', 'A')))), 'tautologie2', "A implique A")
test_if(next(taut2_count), tautologie2(('Equivalent', (('Var', 'A'),('Var', 'A')))), 'tautologie2', "A équivalent à A")

test_if(next(taut2_count), tautologie2(('Vrai', None)), 'tautologie2', "Vrai")

test_if(next(taut2_count), tautologie2(('Non', ('Faux', None))), 'tautologie2', "Non Faux")

f1 = 'Et', (('Non', ('Var', 'A')), ('Var', 'A'))
test_if(next(taut2_count), tautologie2(('Non', f1 )), 'tautologie2', "~(A /\ ~A)")


fin_test_fonction('tautologie2')

# Tests interpret_formule.contradiction2
debut_test_fonction('contradiction2')
print(contradiction2.__doc__)
cont_count2 = gen_int_count()

test_if(next(cont_count2), contradiction2(('Et', (('Var', 'P'),('Non', ('Var', 'P'))))), 'contradiction2', "P /\ ~P")

fin_test_fonction('contradiction2')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('interpret_formule')

# DONE : !!! NE PLUS MODIFIER !!!