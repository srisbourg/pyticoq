import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_context import *
from unit_test_common import *

debut_test_module('type_context')

# Tests type_context.is_tcontext
debut_test_fonction('is_tcontext')
print(is_tcontext.__doc__)
is_tcontext_count = gen_int_count()

b= is_tcontext({})
test_if(next(is_tcontext_count), b, 'is_tcontext')

b= is_tcontext({'toto':('Vrai', None)})
test_if(next(is_tcontext_count), b, 'is_tcontext')

test_exception(next(is_tcontext_count),\
    lambda x : is_tcontext({'toto':('Bouh', None)}),\
         'is_tcontext', "TContextError")


b = is_tcontext({4:('Vrai', None)})
test_ifnot(next(is_tcontext_count), b, 'is_tcontext')

fin_test_fonction('is_tcontext')

# Tests type_context.typecheck_context
debut_test_fonction('typecheck_context')
print(typecheck_context.__doc__)
typecheck_context_count = gen_int_count()

test_no_exception(next(typecheck_context_count), lambda x : typecheck_context({}), 'typecheck_context')
test_no_exception(next(typecheck_context_count), lambda x : typecheck_context({'toto':('Vrai', None)}), 'typecheck_context')
test_exception(next(typecheck_context_count), lambda x : typecheck_context({'toto':('Bouh', None)}), 'typecheck_context')
test_exception(next(typecheck_context_count), lambda x : typecheck_context({4:('Vrai', None)}), 'typecheck_context')

fin_test_fonction('typecheck_context')

# Tests type_context.valid_ident
debut_test_fonction('valid_ident')
print(valid_ident.__doc__)
valid_ident_count = gen_int_count()

c = {'toto':('Vrai', None)}

test_ifnot(next(valid_ident_count), valid_ident('toto', c), 'valid_ident_count')

test_if(next(valid_ident_count), valid_ident('tata', c), 'valid_ident_count')

fin_test_fonction('valid_ident')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
#
#
# fin_test_fonction('FONCTION')

fin_test_module('type_context')
