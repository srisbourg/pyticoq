import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from table import *
from unit_test_common import *
from syntax_formule import *
from printer import *
from interpret_formule import *


debut_test_module('tables')

# Tests interpret_formule.assoc
debut_test_fonction('sous_formules')
#print(sous_formules.__doc__)

#ret = sous_formules(analyse_prop("(P==>Q)==>(Q==>R)==>P==>R"))
#ret = list(ret)
#ret.sort(key=profondeur)
#ret = list(map(lambda f : string_of_formule(f), ret))

#print(str(ret))

#print(enumerate_case(set_variables(analyse_prop("(P==>Q)==>(Q==>R)==>P==>R"))))

#t = table(analyse_prop("(P==>Q)==>(Q==>R)==>P==>R"))

#t = table(analyse_prop("(P==>Q)<==>(~P\\/Q)"))

#t = table(analyse_prop("(P==>Q)<==>~P\\/Q"))

t = table(analyse_prop("P==>Q"))

print(str_square_matrix(t, 10))
#print(t)

#draw('P ==> Q')

fin_test_fonction('sous_formules')


# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('tables')