
import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from syntax_tactic import *
from unit_test_common import *
from syntax_formule import *


debut_test_module('syntax_tactic')

# Tests syntax_tactic.is_coma
debut_test_fonction('is_coma')
print(is_coma.__doc__)
is_COMA_count = gen_int_count()

s = 'sdkfj , kdfls ,dsf'
assert(len(s) == 18)
test_if(next(is_COMA_count), is_coma(s, 6), 'is_coma')
test_if(next(is_COMA_count), is_coma(s, 14), 'is_coma')

fin_test_fonction('is_coma')


# Tests syntax_tactic.is_hyp_aux
debut_test_fonction('is_hyp_aux')
print(is_hyp_aux.__doc__)
is_HYP_aux_count = gen_int_count()

s = 'sdkfj , kd  _fls ,dsf'
assert(len(s) == 21)
h, _ = is_hyp_aux(s, 8)
test_if(next(is_HYP_aux_count), h == 'kd', 'is_hyp_aux')
test_exception2(next(is_HYP_aux_count), lambda x: is_hyp_aux(
    s, 12), 'is_hyp_aux', SyntaxTacticError, 'SyntaxTacticError')


fin_test_fonction('is_hyp_aux')

# Tests syntax_tactic.is_hyp
debut_test_fonction('is_hyp')
print(is_hyp.__doc__)
is_HYP_count = gen_int_count()

s = 'sdkfj , kd  ,fls |dsf'
assert(len(s) == 21)

lh, pos = is_hyp(s, 0)
test_if(next(is_HYP_count), lh == ['sdkfj', 'kd', 'fls'], 'is_hyp')
test_if(next(is_HYP_count), pos == 17, 'is_hyp')

s = 'sdkfj , _kd  ,fls |dsf'
assert(len(s) == 22)
test_exception2(next(is_HYP_aux_count), lambda x: is_hyp(s, 12),\
      'is_hyp', SyntaxTacticError, 'SyntaxTacticError')

fin_test_fonction('is_hyp')

# Tests syntax_tactic.is_tact
debut_test_fonction('is_tact')
print(is_tact.__doc__)
is_TACT_count = gen_int_count()


def testTACT(s, tact, posSta, posRet, syntabs):
    lens = len(s)
    test_if(next(is_TACT_count), is_tact(s, posSta)
            == ((tact, syntabs), posRet), 'is_tact ' + tact)

def testTACTExcept(s, tact, posSta):
    lens = len(s)
    test_exception2(next(is_TACT_count), lambda x: is_tact(s, posSta),
                    'is_tact', SyntaxTacticError, "SyntaxTacticError")


testTACT('split\n', 'split', 0, 6, None)
testTACT('left\n', 'left', 0, 5, None)
testTACT('right\n', 'right', 0, 6, None)

s = 'gg split  \n'
testTACT(s, 'split',3, 11, None)

s = 'ttt left \n'
testTACT(s, 'left', 4, 10, None)

testTACT('fff  right   \n', 'right', 5, 14, None)

testTACTExcept('  \n   \t \n  split f   ', 'split', 11)

testTACT('   intro ghf dksjls', 'intro', 3, 13, 'ghf')

testTACT('fjgh exact jj klrkl', 'exact', 5, 14, 'jj')
testTACT('dks case rlm dgfs', 'case', 4, 13, 'rlm')
testTACT('ff decompose tt fff', 'decompose', 3, 16, 'tt')
testTACT('djshf apply qsd feo', 'apply', 6, 16, 'qsd')

testTACT(' djf intros e,   f  , g   tt  ', 'intros', 5, 26, ['e', 'f', 'g'])

testTACT(' djf   absurd P ==> Q e,   f  , g   tt  ',
         'absurd', 7, 22, analyse_prop('P ==> Q'))

test_exception2(next(is_TACT_count), lambda x: is_tact(' dhfj quit kdfsfgkl', 6),
                'is_tact', EndException, "EndException")

test_exception2(next(is_TACT_count), lambda x: is_tact(' dhfj t kdfsfgkl', 2),
                'is_tact', SyntaxTacticError, "SyntaxTacticError")

fin_test_fonction('is_tact')

# Tests syntax_tactic.analyse_tact
debut_test_fonction('analyse_tact')
print(analyse_tact.__doc__)
analyse_tact_count = gen_int_count()


def testAT(s, syntabs, des=""):
    lens = len(s)
    test_if(next(analyse_tact_count), analyse_tact(
        s) == syntabs, 'analyse_tact', des)


def testATExcept(s, des=""):
    lens = len(s)
    test_exception2(next(analyse_tact_count), lambda x: analyse_tact(s),
                    'analyse_tact' + ("" if des == "" else " " + des),\
                          SyntaxTacticError, "SyntaxTacticError")


# split
testAT('  \n   \t \n  split ', ('split', None),'split')
testATExcept('  \n d  \t \n  split ', 'split')
testATExcept('  \n   \t \n  split f   ', 'split')  

# left
testAT('  \n  \n  left \t   ', ('left', None), 'left')
testATExcept('  \n d  \t \n  left   f ', 'left')

# right
testAT('  \n  right \t   ', ('right', None), 'right')
testATExcept(' e \n  \n  right   f ', 'right') 

# intro
testAT('intro q', ('intro', 'q'), 'intro')
testATExcept('intro _q', 'intro')

# exact
testAT('exact h', ('exact', 'h'), 'exact')

# case
testAT('case h', ('case', 'h'), 'case')

# decompose
testAT('decompose h', ('decompose', 'h'), 'decompose')

# apply
testAT('apply h', ('apply', 'h'), 'apply')

# intros
testAT('intros h0, h1, h2', ('intros', ['h0','h1','h2']), 'intros')
testATExcept('intros ', 'intros')
#s = 'intros h0, _h1, h2'
#print(len(s))
#analyse_tact(s, True)
#print(is_hyp(s, 18, 7))
testATExcept('intros h0, _h1, h2', 'intros')

# absurd
s = 'absurd P==>Q'
testAT(s, ('absurd',analyse_prop('P==>Q')), 'absurd')

s = 'absurd P=>Q'
#print(analyse_tact(s))
testATExcept(s, 'absurd')


# quit
test_exception2(next(analyse_tact_count), lambda x: analyse_tact('quit'),\
     'analyse_tact quit' ,\
          EndException, "EndException")

# Invalid
s = 'absurdd P=>Q'
#print(analyse_tact(s))
testATExcept(s, 'Invalid')

fin_test_fonction('analyse_tact')


# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
#
#
# fin_test_fonction('FONCTION')

fin_test_module('syntax_tactic')
