import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_formule import *
from interpret_formule import *
from syntax_formule import *
from unit_test_common import *

debut_test_module('syntax_formule')

# Test syntax_formule.mkVAR
debut_test_fonction('is_var')
print(is_var.__doc__)
mkV_count = gen_int_count()

s=' dfsd dsfkj kjlk  '
assert(len(s) == 18)
test_if(next(mkV_count), is_var(s, 1) == (('Var', 'dfsd'), 6), 'is_var')
test_if(next(mkV_count), is_var(s, 6) == (('Var', 'dsfkj'), 12), 'is_var')
test_if(next(mkV_count), is_var(s, 12) == (('Var', 'kjlk'), 18), 'is_var')

fin_test_fonction('is_var')


# Tests syntax_formule.is_lpar
debut_test_fonction('is_lpar')
print(is_lpar.__doc__)
is_LPAR_count = gen_int_count()

s = 'sdkfj ( kdfls (dsf'
assert(len(s) == 18)
test_if(next(is_LPAR_count), is_lpar(s, 6), 'is_lpar')
test_if(next(is_LPAR_count), is_lpar(s, 14), 'is_lpar')


fin_test_fonction('is_lpar')


# Tests syntax_formule.is_rpar
debut_test_fonction('is_rpar')
print(is_rpar.__doc__)
is_RPAR_count = gen_int_count()

s = 'sdkfj ) kdfls )dsf'
assert(len(s) == 18)
test_if(next(is_RPAR_count), is_rpar(s, 6), 'is_rpar')
test_if(next(is_RPAR_count), is_rpar(s, 14), 'is_rpar')

fin_test_fonction('is_rpar')


# Tests syntax_formule.is_and
debut_test_fonction('is_and')
print(is_and.__doc__)
is_AND_count = gen_int_count()
test_if(next(is_AND_count), is_and('/\\', 0), 'is_and')
test_if(next(is_AND_count), is_and('P /\\ Q', 2), 'is_and')

fin_test_fonction('is_and')


# Tests syntax_formule.is_or
debut_test_fonction('is_or')
print(is_or.__doc__)
is_OR_count = gen_int_count()
test_if(next(is_OR_count), is_or('\\/', 0), 'is_or')
test_if(next(is_OR_count), is_or('P \\/ Q', 2), 'is_or')

fin_test_fonction('is_or')


# Tests syntax_formule.is_not
debut_test_fonction('is_not')
print(is_not.__doc__)
is_NOT_count = gen_int_count()

s = 'dkfj~sfdkln ~ldf    ~ dsf'

test_if(next(is_NOT_count), is_not(s, 4), 'is_not')
test_if(next(is_NOT_count), is_not(s, 12), 'is_not')
test_if(next(is_NOT_count), is_not(s, 20), 'is_not')

fin_test_fonction('is_not')


# Tests syntax_formule.is_imp
debut_test_fonction('is_imp')
print(is_imp.__doc__)
is_IMP_count = gen_int_count()

s = 'dkfj==>sfdkln ==>ldf    ==> dsf'

test_if(next(is_IMP_count), is_imp(s, 4), 'is_imp')
test_if(next(is_IMP_count), is_imp(s, 14), 'is_imp')
test_if(next(is_IMP_count), is_imp(s, 24), 'is_imp')

fin_test_fonction('is_imp')


# Tests syntax_formule.is_equ
debut_test_fonction('is_equ')
print(is_equ.__doc__)
is_EQU_count = gen_int_count()

s = 'dkfj<==>sfdkln <==>ldf    <==> dsf'

test_if(next(is_EQU_count), is_equ(s, 4), 'is_equ')
test_if(next(is_EQU_count), is_equ(s, 15), 'is_equ')
test_if(next(is_EQU_count), is_equ(s, 26), 'is_equ')

fin_test_fonction('is_equ')


# Tests syntax_formule.is_vrai
debut_test_fonction('is_vrai')
print(is_vrai.__doc__)
is_Vrai_count = gen_int_count()

s = ' dskflskd Vrai dsf Vrai'
assert(len(s)==23)
test_if(next(is_Vrai_count), is_vrai(s, 10), 'is_vrai')

v, p = is_vrai(s, 10)
test_no_exception(next(is_Vrai_count), lambda x : typecheck_formule(v), 'is_vrai')
test_if(next(is_Vrai_count), v==('Vrai', None), 'is_vrai')
test_if(next(is_Vrai_count), p==15, 'is_vrai')
test_if(next(is_Vrai_count), is_vrai(s, 19), 'is_vrai')

fin_test_fonction('is_vrai')


# Tests syntax_formule.is_faux
debut_test_fonction('is_faux')
print(is_faux.__doc__)
is_Faux_count = gen_int_count()

s = ' dskflskd Faux dsf Faux'
assert(len(s)==23)
test_if(next(is_Faux_count), is_faux(s, 10), 'is_faux')

v, p = is_faux(s, 10)
test_no_exception(next(is_Faux_count), lambda x : typecheck_formule(v), 'is_faux')
test_if(next(is_Faux_count), v==('Faux', None), 'is_faux')
test_if(next(is_Faux_count), p==15, 'is_faux')
test_if(next(is_Faux_count), is_faux(s, 19), 'is_faux')

fin_test_fonction('is_faux')


# Tests syntax_formule.is_bool
debut_test_fonction('is_bool')
print(is_bool.__doc__)
is_BOOL_count = gen_int_count()

s = ' dskflskd Faux dsf Vrai'
assert(len(s)==23)
test_if(next(is_BOOL_count), is_bool(s, 10), 'is_bool')

v, p = is_bool(s, 10)
test_no_exception(next(is_BOOL_count), lambda x : typecheck_formule(v), 'is_bool')
test_if(next(is_BOOL_count), v==('Faux', None), 'is_bool')
test_if(next(is_BOOL_count), p==15, 'is_bool')
test_if(next(is_BOOL_count), is_bool(s, 19), 'is_bool')

fin_test_fonction('is_bool')


# Tests interpret syntax_formule.is_form0
debut_test_fonction('is_form0')
print(is_form0.__doc__)
is_FORM0_count = gen_int_count()

str_form = 'P1 <==> Q'
assert(len(str_form)==9)

res = 'Equivalent', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM0_count), is_form0(str_form, 0) == (res, 9), 'is_form0' )

fin_test_fonction('is_form0')


# Tests interpret syntax_formule.is_form1
debut_test_fonction('is_form1')
print(is_form1.__doc__)
is_FORM1_count = gen_int_count()

str_form = 'P1 ==> Q'
assert(len(str_form)==8)

res = 'Implique', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM1_count), is_form1(str_form, 0) == (res , 8), 'is_form1' )

fin_test_fonction('is_form1')


# Tests interpret syntax_formule.is_form2
debut_test_fonction('is_form2')
print(is_form2.__doc__)
is_FORM2_count = gen_int_count()

str_form = 'P1 \/ Q'
assert(len(str_form)==7)

res = 'Ou', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM2_count), is_form2(str_form, 0) == (res, 7), 'is_form2' )

fin_test_fonction('is_form2')


# Tests interpret syntax_formule.is_form3
debut_test_fonction('is_form3')
print(is_form3.__doc__)
is_FORM3_count = gen_int_count()

str_form = 'P1 /\\ Q'
assert(len(str_form)==7)

res = 'Et', (('Var', 'P1'), ('Var', 'Q'))

#print(str(is_form3(str_form, 7, 0) ))

test_if(next(is_FORM3_count), is_form3(str_form, 0) == (res, 7), 'is_form3' )

fin_test_fonction('is_form3')


# Tests interpret syntax_formule.is_form4
debut_test_fonction('is_form4')
print(is_form4.__doc__)
is_FORM4_count = gen_int_count()

str_form = '(~P1 \/ P2) /\\ Q'
assert(len(str_form)==16)

#print(str(is_form4(str_form, 16, 1) ))
res = 'Ou', (('Non', ('Var', 'P1')), ('Var', 'P2'))

test_if(next(is_FORM4_count), is_form4(str_form, 1) == (('Non', ('Var', 'P1')), 5), 'is_form4' )
test_if(next(is_FORM4_count), is_form4(str_form, 0) == (res, 12), 'is_form4' )

fin_test_fonction('is_form4')


# Tests interpret syntax_formule.is_form5
debut_test_fonction('is_form5')
print(is_form5.__doc__)
is_FORM5_count = gen_int_count()

str_form = '(P1 \/ Vrai) /\\ Q'
assert(len(str_form)==17)

res0 = 'Ou', (('Var', 'P1'), ('Vrai', None))

#print(str(is_form5(str_form, 17, 0)))

test_if(next(is_FORM5_count), is_form5(str_form, 16) == (('Var', 'Q'), 17) , 'is_form5' )
test_if(next(is_FORM5_count), is_form5(str_form, 7) == (('Vrai', None), 11) , 'is_form5' )
test_if(next(is_FORM5_count), is_form5(str_form, 0) == (res0, 13) , 'is_form5' )

fin_test_fonction('is_form5')


# Tests interpret syntax_formule.is_form_par
debut_test_fonction('is_form_par')
print(is_form_par.__doc__)
is_FORM_PAR_count = gen_int_count()

str_form = '(~P1 \/ P2) /\\ Q'
assert(len(str_form)==16)

res = 'Ou', (('Non', ('Var', 'P1')), ('Var', 'P2'))

test_if(next(is_FORM_PAR_count), is_form_par(str_form, 0) == (res, 12), 'is_form_par' )

fin_test_fonction('is_form_par')


# Tests interpret syntax_formule.analyse_prop
debut_test_fonction('analyse_prop')
print(analyse_prop.__doc__)
analyse_prop_count = gen_int_count()

ret = analyse_prop('P1         ')
test_no_exception(next(analyse_prop_count), lambda x : test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' ), 'analyse_prop' )
oracle = ('Var', 'P1')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('          _S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Var', '_S')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('      (E)    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Var', 'E')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('      ~(E)    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Non', ('Var', 'E'))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' ~~E    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Non', ('Non', ('Var', 'E')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P1 \n /\\ Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P1 \n /\\ Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('~E/\\P')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Non', ('Var', 'E')), ('Var', 'P')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P/\\Q \\/ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Ou', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P/\\ Q /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P'), ('Et', (('Var', 'Q'), ('Var', 'S')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P\\/ Q /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Ou', (('Var', 'P'), ('Et', (('Var', 'Q'), ('Var', 'S')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P\\/ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Ou', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'R')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

test_exception(next(analyse_prop_count), lambda x : analyse_prop(' (P/\\ Q) = R'), 'analyse_prop' )
        
ret = analyse_prop(' Q ==> P ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Var', 'Q'), ('Implique', (('Var', 'P'), ('Var', 'R')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (Q ==> P) ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Implique', (('Var', 'Q'), ('Var', 'P'))), ('Var', 'R')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P1 <==> Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' ~P <==> Q ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent', (('Non', ('Var', 'P')), ('Implique', (('Var', 'Q'), ('Var', 'R')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P ==> Q <==> ~P \\/Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent',
 (('Implique', (('Var', 'P'), ('Var', 'Q'))),
  ('Ou', (('Non', ('Var', 'P')), ('Var', 'Q')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

test_if(next(analyse_prop_count), tautologie(analyse_prop(' P ==> Q <==> ~P \\/Q')), 'analyse_prop' )

test_if(next(analyse_prop_count), tautologie2(analyse_prop(' P ==> Q <==> ~P \\/Q')), 'analyse_prop' )

fin_test_fonction('analyse_prop')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('syntax_formule')