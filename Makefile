SRC_DIR = src
LIB_DIR = lib
TEST_DIR = tests
HTML_DIR = html
ML_DIR = ml
IMG_DIR = img
MD_DIR = md
ML_FILES = $(ML_DIR)/assistant.ml
FILES = type_formule interpret_formule syntax_formule type_tactic type_context type_goal printer pyticoq syntax_tactic syntax_common table circuit
IPYNB_FILES = $(addsuffix .ipynb, $(FILES))
SRC_FILES = $(addprefix $(SRC_DIR)/, $(IPYNB_FILES))
PY_FILES = $(addprefix $(LIB_DIR)/, $(addsuffix .py, $(FILES)))
HTML_FILES = $(addprefix $(HTML_DIR)/, $(addsuffix .html, $(FILES)))
MD_FILES = $(addprefix $(MD_DIR)/, $(addsuffix .md, $(FILES)))
TEST_FILES = $(addprefix $(TEST_DIR)/test_, $(addsuffix .py, $(FILES))) $(TEST_DIR)/unit_test_common.py
PYTHON = python3.9
JUPYTER = ~/.local/bin/jupyter
ARCHIVES = $(SRC_FILES) $(TEST_FILES) $(HTML_FILES) $(PY_FILES) $(ML_FILES) Makefile README.md README.pdf img checksum
PROJECT_NAME = projetcapes2021SamuelRisbourg
TAR = $(PROJECT_NAME).tar.gz
TMP_DIR = /tmp
TMP_TEST_DIR = $(TMP_DIR)/$(PROJECT_NAME)
CURRENT_TEST_FILE = $(TEST_DIR)/test_table.py
PYLINT = /home/sam/.local/bin/pylint
#PYLINT = pylint
VERSION = 0.2
TAG = v$(VERSION)
TAG_NAME = pyticoq-$(TAG)
TAGTAR = $(TAG_NAME).tar.gz
TAGURL = https://gitlab.com/srisbourg/pyticoq/-/archive/$(TAG)/$(TAGTAR)
TMP_TAG_DIR =  $(TMP_TEST_DIR)/$(TAG)
RELEASE_TAG_TAR = $(PROJECT_NAME)_$(TAG).tar.gz

ALL: $(PY_FILES)

git_tag: release/$(RELEASE_TAG_TAR)
	git push --delete origin $(TAG)
	git tag -a $(TAG) -fm 'Pyticoq $(TAG)'
	git push --follow-tags


checksum: $(SRC_FILES) $(TEST_FILES) $(HTML_FILES) $(PY_FILES) $(ML_FILES) Makefile README.md
	rm -f pyticoq
	touch pyticoq
	md5sum $^ >> pyticoq
	md5sum pyticoq > checksum
	cat checksum

$(PY_FILES): $(SRC_FILES)
	@$(JUPYTER) nbconvert --to python $^ --output-dir $(LIB_DIR)
	@echo "Compiled "$^" successfully!"
	@for file in $(LIB_DIR)/*.py ; do \
		sed -i 's/ipynb.fs.full.//g' $${file}  ;  $(PYTHON) src/clean_proj.py ;	reindent $${file}  ;\
	done; 

$(HTML_FILES): $(SRC_FILES)
	@$(JUPYTER) nbconvert --to html $^ --output-dir $(HTML_DIR)
	@echo "Compiled "$^" successfully!"

mk_html: $(HTML_FILES)

$(MD_FILES) : $(SRC_FILES)
	@$(JUPYTER) nbconvert --to markdown $^ --output-dir $(MD_DIR)
	@echo "Compiled "$^" successfully!"

mk_markdown : $(MD_FILES)

html : $(HTML_FILES)

current_test: $(PY_FILES) $(CURRENT_TEST_FILE)
	@clear; cd $(TEST_DIR) ; \
	$(PYTHON) `basename $(CURRENT_TEST_FILE)`

tests: $(PY_FILES)
	@clear; cd $(TEST_DIR) ; \
	for file in *.py ; do \
		$(PYTHON) `basename $${file}`  ; \
	done

tests_check: $(PY_FILES)
	@clear; cd $(TEST_DIR) ; \
	for file in *.py ; do \
		$(PYTHON) `basename $${file}`  ; read foo; clear; \
	done

# FIXME
# $(PYTHON) $(LIB_DIR)/pyticoq.py --formule "((A==>Q)/\(B==>Q))==>A\/B==>Q" --tactics "intros h1, h2; decompose h1; case h2; apply _hyp0; exact _hyp2; apply _hyp1; exact _hyp3"

tests_pyticoq: $(PY_FILES)
	$(PYTHON) $(LIB_DIR)/pyticoq.py --formule "P==>P" --tactics "intro h; exact h"
	$(PYTHON) $(LIB_DIR)/pyticoq.py --formule "(P==>Q)==>(Q==>R)==>P==>R" --tactics "intros h1, h2, h3; apply h2; apply h1; exact h3"
	$(PYTHON) $(LIB_DIR)/pyticoq.py --formule "(A\/B==>Q)==>A==>Q" --tactics "intros h1, h2; apply h1; left; exact h2"
	$(PYTHON) $(LIB_DIR)/pyticoq.py --formule "(A/\B==>Q)==>A==>B==>Q" --tactics "intros h1, h2, h3; apply h1; split; exact h2; exact h3"
	$(PYTHON) $(LIB_DIR)/pyticoq.py --formule "A/\~A==>False" --tactics "intro h; decompose h; absurd A; exact _hyp0; exact _hyp1"

tests_table: $(PY_FILES)
	$(PYTHON) $(LIB_DIR)/table.py "P==>P"
	$(PYTHON) $(LIB_DIR)/table.py "(P==>Q)==>(Q==>R)==>P==>R" 40
	$(PYTHON) $(LIB_DIR)/table.py "(A\/B==>Q)==>A==>Q" 30
	$(PYTHON) $(LIB_DIR)/table.py "(A/\B==>Q)==>A==>B==>Q" 40
	$(PYTHON) $(LIB_DIR)/table.py "A/\~A==>False"

test_circuit: $(PY_FILES)
	$(PYTHON) $(LIB_DIR)/circuit.py "P==>Q"

release/$(RELEASE_TAG_TAR): $(TAR)
	cp $< $@
	git add $@
	git commit -m 'Pyticoq CAFEP 2021 $(TAG)'

$(TAR): $(ARCHIVES)
	tar -zcvf $@ $^

tartest: $(TAR)
	rm -Rf $(TMP_TEST_DIR)
	mkdir $(TMP_TEST_DIR)
	tar xvzf $(TAR) -C $(TMP_TEST_DIR)
	make -C $(TMP_TEST_DIR) tests
	make -C $(TMP_TEST_DIR) tests_pyticoq

$(TAGTAR):
	wget $(TAGURL)

tagtest: $(TAGTAR)
	rm -Rf $(TMP_TAG_DIR)
	mkdir $(TMP_TAG_DIR)
	tar xvzf $< -C $(TMP_TAG_DIR)
	make -C $(TMP_TAG_DIR)/$(TAG_NAME) tests
	make -C $(TMP_TAG_DIR)/$(TAG_NAME) tests_pyticoq

compare_checksum: tartest tagtest
	rm -f $(TMP_TAG_DIR)/$(TAG_NAME)/.gitignore
	make -C $(TMP_TEST_DIR) checksum
	make -C $(TMP_TAG_DIR)/$(TAG_NAME) checksum
	diff $(TMP_TEST_DIR)/checksum $(TMP_TAG_DIR)/$(TAG_NAME)/checksum

README.md : lint
	if [ -f ".gitignore" ]; then \
		git checkout $@; \
	fi ; \
	echo >> $@; echo >> $@; echo "# Métriques" >> $@ ;\
	echo >> $@; echo >> $@; echo "## Pyticoq" >> $@ ;\
	cloc --md --hide-rate lib >> $@;\
	echo >> $@; echo >> $@; echo "## Tests et Makefile" >> $@ ;\
	cloc --md --hide-rate tests Makefile >> $@;\
	echo >> $@; echo >> $@; echo "## Pylint" >> $@ ;\
	sed -i 's/github.com\/AlDanial\/cloc v 1.81//g' $@; \
	sed -i 's/cloc//g' $@; \
	tail -n 2 lint.log >> $@; \
	

# TODO/FIXME : KO à cause des symbole logiques en Markdown
# Reste à fixer les caractères spéciaux
README.pdf : README.html
	htmldoc --webpage --charset utf-8 $< --outfile $@
	if [ -f ".gitignore" ]; then \
		git checkout README.md; \
	fi

README.html : README.md
	pandoc -s $< -o $@

lint: $(PY_FILES)
	@$(PYLINT) $^ > lint.log; tail -n 2 lint.log

#TODO debug et manuel
ocamlproj:
	ocaml $(ML_DIR)/assistant.ml

run: $(PY_FILES)
	$(PYTHON) $(LIB_DIR)/pyticoq.py

clean:
	@rm -f $(LIB_DIR)/*.py
	@rm -Rf $(LIB_DIR)/__pycache__
	@rm -Rf $(TEST_DIR)/__pycache__
	@rm -Rf $(SRC)/.ipynb_checkpoints
	@rm -f $(TAR)
	@rm -f $(HTML_DIR)/*.html
	@rm -Rf .ipynb_checkpoints
	@rm -f lint.log
	@git checkout README.md
	@rm -f README.pdf
	@rm -f README.html
	@rm -f README.tex
	@rm -f README.aux
	@rm -f README.log
	@rm -f pyticoq
	@rm -f checksum
	@rm -f pyticoq-v0.1.tar.gz


.PHONY: clean tests $(TAR) tartest current_test lint tests_check README.md README.pdf README.html mk_html mk_markdown README.tex checksum release/$(RELEASE_TAG_TAR) git_tag