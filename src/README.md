Index des modules
===

# Vérification de types

## type_context

Module de définition et de vérification du type context

* is_tcontext(context)

* TContextError

* typecheck_context(ctx)

* valid_ident(ident, context)


## type_formule

Fonctions de vérification du type tformule (Formule logique)

* def is_tformule (tformule)

* TypeCheckFormuleError

* typecheck_formule(tformule)


## type_goal

Module de définition et de vérification du type goal

* is_tgoal(goal)

* TGoalError

* typecheck_goal(goal)

* mk_goal(context, formule)


## type_tactic

Module de vérification de tuple du langage de tactique

* is_ttactic (tactic)

* TypeCheckTacticError

* typecheck_tactic(tact)


# Syntaxes

## syntax_common

Fonctions d'analyse syntaxique communes aux deux langages

*  ignore_char(c)

*  debut_mot(s, pos)

*  def fin_chaine(s, pos)

*  SyntaxFormuleError

*  is_keyword(keyword, s, pos, e=SyntaxFormuleError)

* possible(c)

* is_ident(s, pos, e=SyntaxFormuleError)


## syntax_formule

Fonctions d'analyse syntaxique de formule

* is_VAR(s, pos)

* is_lpar(s, pos)

* is_and(s, pos)

* is_or(s, pos)

* is_imp(s, pos)

* is_equ(s, pos)

* is_vrai(s, pos)

* is_faux(s, pos)

* is_bool(s, pos)

* analyse_prop(s)

* def is_form0(s, pos)

* is_form1(s, pos)

* is_form2(s, pos)

* is_form3(s, pos)

* is_form4(s, pos)

* is_form5(s, pos)

* is_form_par(s, pos)


## syntax_tactic

Module pour l'analyse syntaxique du langage de tactiques

* SyntaxTacticError

* is_coma(s, pos)

* is_hyp_aux(s, pos)

* is_hyp(s, pos)

* EndException

* is_tact(s, pos)

* analyse_tact(s)


# Tables de vérité et tautologie (interpret_formule)

* AssocError

* assoc (cle, dico)

* EvalError

* eval_tf (dict_assoc, tformule)

* set_variables (tformule)

* enumerate_case (set_vars)

* tautologie(formule)

* contradiction(formule)

* SubstError

* substitution (var, valeur, tformule)

* tautologie2(formule)

* contradiction2(formule)


# Affichage (printer)

* formule_priority(tformule)

* StringFormuleError

* string_of_formule(tformule)

* parent_string_of_formule(formule, priority, assoc)

* print_formule(tformule)

* string_of_hyp (ident, formule)

* string_of_context(context)

* string_of_goal(goal)

* fresh_ident(prefix='_hyp')

* StringTacticError

* string_of_tactic(tactic)


# Programme principal (pyticoq)

* IdentInvalidError

* TacticError

* introduction (hyps, context, formule)

* conditions_apply(phi, formule)

* apply_tactic(tactic, goal, gen_fresh_ident)

* step(goal, gen_fresh_ident, strtact=None)

* interactive_loop(goals, arrstrtactics=None)

* pyticoq(strformule = None, tactics = None)